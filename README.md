# send2remarkable

Scripts for uploading files to reMarkable 2 tablet over SSH (with or without USB ethernet server)

Should work on both Linux and macOS systems, but only tested with Ubuntu

These scripts are currently taken near-wholesale from the pdf2remarkable script at https://github.com/adaerr/reMarkableScripts. The only notable differences are:

- **automated thumbnail generation and the related ImageMagick dependency have been removed**  
    xochitl itself appears to automatically generate a thumbnail/preview image when it picks up on a new file without one; this may be specific to reMarkable 2 tablet or a relatively recent reMarkable software update, I'm not sure

- **duplicated and tweaked the script so there is one that can be used for uploading EPUBs as well as PDFs**  
    this was pretty much just a matter of editing file extension and variable names accordingly

To use:
- download or clone this repository
- edit scripts to make sure they're executable (e.g. `chmod +x [epub2remarkable.sh]`)
- edit local `.ssh/config` so that your tablet's local IP address is assigned hostname `remarkable`
    - example configs are provided in each script
    - you can assign this to either the reMarkable's built-in ethernet server at `10.11.99.1` if connected over USB, or whatever private IP is given to the tablet by your router; I recommend assigning your reMarkable a static private IP on your router if you want to go the latter route
- run e.g. `./epub2remarkable -r /path/to/file.epub` on the file you want to upload
    - the `-r` flag automatically restarts the xochitl service on the tablet once the transfer is done, which is necessary for you to actually see and start using the file; you can omit this flag if you want to make a series of uploads first, or reboot your tablet manually for whatever reason

## License
As indicated in the original script, Adrian Daerr intended their work to be "public domain". Accordingly, I am putting out any changes I have made and any edits to this repostiory as CC0, to the best of my ability waiving any claim to copyright on further tinkering here. See attached license file.
